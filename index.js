const fetch = require("node-fetch");
const path = require("path");
const querystring = require("querystring");
const { createFile, createCSVFile, createLog } = require("./util");

async function fetchContentful(space, environment, options) {
  const url = (space, environment) =>
    `https://cdn.contentful.com/spaces/${space}/environments/${environment}/entries`;
  return fetch(
    `${url(space, environment)}?${querystring.stringify(options)}`
  ).then((res) => res.json());
}

// Get list
function getListOfTopicContent(skip = 0) {
  return fetchContentful("vrrt8fsfwf0e", "master", {
    content_type: "topicContent",
    limit: 100,
    skip,
    "fields.practiceSetQuestionId[exists]": true,
    access_token: "pc0G0yt2LZ5Yqlz8eUPB-n4iJ9Uic9vuxp3gxH7Q4ZM",
  });
}

// WRITE TO LOG
function writeToLog(stream, items) {
  items.forEach((item) => {
    if (item.sys) {
      try {
        const { id } = item.sys;
        const {
          title,
          subject,
          classification,
          topic,
          practiceSetQuestionId,
        } = item.fields;

        stream.write([
          id,
          title,
          subject,
          classification,
          topic,
          practiceSetQuestionId,
        ]);
      } catch (e) {}
    } else {
      console.log("error", item);
    }
  });
}

// LOOP THROUGH THE DATA
async function recursiveRun(stream, skip = 0) {
  const data = await getListOfTopicContent(skip);
  const newTotal = data.total;
  const limit = data.limit;
  const newLeft = newTotal - limit;

  writeToLog(stream, data.items);

  if (newLeft > 0) {
    return recursiveRun(stream, skip + limit);
  }

  return true;
}

// RUN
async function run() {
  const date = new Date();
  const dateFormatted = [
    date.getFullYear(),
    date.getMonth() + 1,
    date.getDate(),
  ].join("-");

  const outputPath = path.join(__dirname, `./out`);

  const stream = await createCSVFile(
    `${outputPath}/${dateFormatted}-list.csv`,
    [
      "entryId",
      "title",
      "subject",
      "classification",
      "topic",
      "practiceSetQuestionId",
    ]
  );

  await recursiveRun(stream);

  console.log("done");
}

run();
