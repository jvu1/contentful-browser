/* eslint-disable @typescript-eslint/no-var-requires */
const fs = require('fs');

/**
 * Delete File
 * @param {*} path
 * @param {*} rejectOnError
 */
function deleteFile(path, rejectOnError = true) {
  return new Promise((resolve, reject) => {
    fs.stat(path, (err, stats) => {
      if (rejectOnError && err) {
        return reject(err);
      }

      return fs.unlink(path, unlinkErr => {
        if (rejectOnError && unlinkErr) return reject(unlinkErr);
        return resolve({ deletedFile: stats });
      });
    });
  });
}

/**
 * Create File
 * @param {*} path
 */
async function createFile(path) {
  // Delete the file first
  await deleteFile(path, false);

  // Create file
  const stream = fs.createWriteStream(path, {
    flags: 'a'
  });

  return {
    write(str) {
      stream.write(str.toString());
      stream.write('\n');
      return this;
    }
  };
}

/**
 * Create CSV File
 * @param {*} path
 * @param {*} headers
 */
async function createCSVFile(path, headers) {
  // Create File
  const stream = await createFile(path);

  // Write up headers
  stream.write(`${headers.join(',')}`);

  return {
    write(arr) {
      if (arr.length !== headers.length) {
        throw new Error('Incorrect amount of datapoints in this entity');
      }

      stream.write(`${arr.join(',')}`);

      return this;
    }
  };
}

/**
 * Log
 * @param {} path
 */
async function createLog(path) {
  // Create File
  const stream = await createFile(path);

  return (message, ...args) => {
    const msg = `> ${message}`;

    stream.write(msg);
    stream.write(args);

    return console.log(msg, ...args);
  };
}

module.exports = {
  createLog,
  deleteFile,
  createFile,
  createCSVFile
};
